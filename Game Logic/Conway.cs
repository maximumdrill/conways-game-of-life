﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ConwaysGameOfLife.Game_Logic
{
    public class Conway
    {
        bool[,] Data;
        int width, height;
        public Conway(int _width, int _height)
        {
            width = _width;
            height = _height;
            Data = new bool[width, height];
        }

        public bool[,] GetGameData()
        {
            bool[,] clone = new bool[width, height];

            for(int i = 0; i < height; i++)
            {
                for(int j = 0; j < width; j++)
                {
                    if(Data[j, i])
                    {
                        clone[j, i] = GetNeighborCount(j, i) == 2 || GetNeighborCount(j, i) == 3;
                    } else
                    {
                        clone[j, i] = GetNeighborCount(j, i) == 3;
                    }
                }
            }
            return Data = clone;
        }

        private int GetNeighborCount(int xLocation, int yLocation)
        {
            int totalNeighbors = 0;

            totalNeighbors += CheckLocation(xLocation, yLocation - 1);
            totalNeighbors += CheckLocation(xLocation - 1, yLocation);
            totalNeighbors += CheckLocation(xLocation + 1, yLocation);
            totalNeighbors += CheckLocation(xLocation, yLocation + 1);

            totalNeighbors += CheckLocation(xLocation - 1, yLocation - 1);
            totalNeighbors += CheckLocation(xLocation - 1, yLocation + 1);
            totalNeighbors += CheckLocation(xLocation + 1, yLocation - 1);
            totalNeighbors += CheckLocation(xLocation + 1, yLocation + 1);

            return totalNeighbors;
        }

        private int CheckLocation(int x, int y)
        {
            if (x >= 0 && x < width)
            {
                if (y >= 0 && y < height)
                {
                    if(Data[x, y])
                    {
                        return 1;
                    }
                }
            }
            return 0;
        }

        internal void MatchGrids(Rectangle[,] grid, SolidColorBrush populated)
        {
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    Data[j, i] = grid[j, i].Fill.Equals(populated);
                }
            }
        }
    }
}
