﻿using ConwaysGameOfLife.Game_Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ConwaysGameOfLife
{
    public partial class MainWindow : Window
    {

        Conway GameLogic;
        Canvas MainCanvas;
        Rectangle[,] Grid;
        SolidColorBrush Black, White;
        const int GRID_WIDTH = 97, GRID_HEIGHT = 40, GRID_SPACE_SIZE = 10;
        int GenerationCounter, GameSpeed;
        bool Run;

        public MainWindow()
        {
            InitializeComponent();
            MainCanvas = conwayCanvas;
            GameLogic = new Conway(GRID_WIDTH, GRID_HEIGHT);
            Black = new SolidColorBrush(Color.FromRgb(0, 0, 0));
            White = new SolidColorBrush(Color.FromRgb(255, 255, 255));
            Grid = new Rectangle[GRID_WIDTH, GRID_HEIGHT];
            GenerationCounter = 0;
            GameSpeed = (int) speedSlider.Value;
            PopulateGrid();
        }

        private void PopulateGrid()
        {
            int ThicknessX = 0, ThicknessY = 0;
            for (int i = 0; i < GRID_HEIGHT; i++)
            {
                for (int j = 0; j < GRID_WIDTH; j++)
                {
                    Grid[j, i] = new Rectangle
                    {
                        Height = GRID_SPACE_SIZE,
                        Width = GRID_SPACE_SIZE,
                        Fill = Black,
                        Margin = new Thickness(ThicknessX, ThicknessY, 0, 0)
                    };
                    Grid[j, i].MouseLeftButtonDown += RectangleClick;
                    MainCanvas.Children.Add(Grid[j, i]);
                    ThicknessX += 10;
                }
                ThicknessX = 0;
                ThicknessY += 10;
            }

        }

        private void stopGame_Click(object sender, RoutedEventArgs e)
        {
            Run = false;
            ResetGame();
        }

        internal void ResetGame()
        {
            for (int i = 0; i < GRID_HEIGHT; i++)
            {
                for (int j = 0; j < GRID_WIDTH; j++)
                {
                    Grid[j, i].Fill = Black;
                }
            }
            GenerationCounter = 0;
            generationCounter.Content = "Generations: " + GenerationCounter;
            GameLogic.MatchGrids(Grid, White);
        }

        private void speedSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            GameSpeed = (int)speedSlider.Value;
        }

        private void RectangleClick(object sender, MouseButtonEventArgs e)
        {
            ((Rectangle)e.Source).Fill = (((Rectangle)e.Source).Fill == White) ? Black : White;
            GameLogic.MatchGrids(Grid, White);
        }

        private async void startGame_Click(object sender, RoutedEventArgs e)
        {
            GameSpeed = (int)speedSlider.Value;
            Run = true;
            do
            {
                await Task.Delay(GameSpeed);
                BuildFrame(GameLogic.GetGameData());
                GenerationCounter += 1;
                generationCounter.Content = "Generations: " + GenerationCounter;
            } while (Run);
            ResetGame();
        }

        public void BuildFrame(bool[,] ConwayData)
        {
            for (int i = 0; i < GRID_HEIGHT; i++)
            {
                for (int j = 0; j < GRID_WIDTH; j++)
                {
                    Grid[j, i].Fill = (ConwayData[j, i]) ? White : Black;
                }
            }
        }
    }
}
